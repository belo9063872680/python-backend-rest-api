from django.db import models

# Create your models here.


class Author(models.Model):
    first_name = models.CharField(verbose_name="Имя", max_length=40)
    last_name = models.CharField(verbose_name="Фамилия", max_length=40)

    def __str__(self):
        return self.last_name

    class Meta:
        verbose_name = "Автор"
        verbose_name_plural = "Авторы"


class Genre(models.Model):
    title = models.CharField(verbose_name="Жанр", max_length=150, default='')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Жанр"
        verbose_name_plural = "Жанр"


class Book(models.Model):
    title = models.CharField(verbose_name="Название",
                             max_length=50, default='')
    publication_year = models.IntegerField(
        verbose_name='Год издания')
    author = models.ForeignKey(Author, on_delete=models.PROTECT)
    genre = models.ForeignKey(Genre, on_delete=models.PROTECT)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Книга"
        verbose_name_plural = "Книги"
