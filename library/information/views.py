from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from .models import *
from .serializers import *
# Create your views here.


class AuthorViewSet(ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    # authentication_classes = (TokenAuthentication)
    # permission_classes = (IsAuthenticated)


class BookViewSet(ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    # authentication_classes = (TokenAuthentication)
    # permission_classes = (IsAuthenticated)


class GenreViewSet(ModelViewSet):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer
    # authentication_classes = (TokenAuthentication)
    # permission_classes = (IsAuthenticated)
